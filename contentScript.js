(() => {
    // let youtubeLeftControls, youtubePlayer;
    let currentVideo = "";

    chrome.runtime.onMessage.addListener((obj, sender, response) => {
        const { type, value, videoId } = obj;

        if (type === "NEW") {
            currentVideo = videoId;
            newVideoLoaded();
        }
    });

    // let isActive = false
    let isHidden = false

    const currentVideoLoaded = () => {
        // const bookmarkBtnExists = document.getElementsByClassName("bookmark-btn")[0];

        document.addEventListener("keydown", (e) => {
            const auto_play = document.querySelector('#movie_player > div.ytp-chrome-bottom > div.ytp-chrome-controls > div.ytp-right-controls > button:nth-child(2) > div > div');
            // let movie_player = document.querySelector('#movie_player')
            // let control = document.querySelector('#movie_player > div.ytp-chrome-bottom')

            elements = [
                '#movie_player > div.ytp-chrome-top > button > div.ytp-playlist-menu-button-icon',
                '#movie_player > div.ytp-chrome-top > div.ytp-chrome-top-buttons',
                '#movie_player > div.ytp-chrome-top > div.ytp-title > div > a',
                '#movie_player > div.ytp-chrome-top > button',
                '#movie_player > div.ytp-chrome-bottom',
                '.ytp-gradient-bottom',
                '.ytp-gradient-top',
            ].map(el => {
                return document.querySelector(el)
            });

            console.log(elements[0])
            if (e.key === 'h' && !(document.activeElement === document.querySelector('input'))) {
                if (auto_play) {
                    if (auto_play.ariaChecked == 'true') {
                        auto_play.ariaChecked = 'false';
                    }
                }
                if (isHidden) {
                    elements.forEach(el => {
                        if (el)
                            el.style.display = "block";
                    });
                    isHidden = false
                }
                else {
                    elements.forEach(el => {
                        if (el)
                            el.style.display = "none";
                    });
                    isHidden = true
                }
            }
        });
    }
    currentVideoLoaded();
})();

